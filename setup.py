# -*- coding: utf-8 -*-
"""
Script Python de Movin'Smart
Auteur : Léandre Varennes 
Création du fichier le "17/04/2024$"
"""

from setuptools import setup, find_packages


setup(
    name='package01',
    version='0.1',
    packages=find_packages(),
    install_requires=[
        'setuptools==69.5.1'
        ],
    # autres métadonnées comme l'auteur, la description, etc.
)

